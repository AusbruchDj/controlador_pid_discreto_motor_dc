%% PARAMETROS DE LA PLANTA (MOTOR DC)
R=0.008;    %Resistencia 
L=1;        %Inductancia 
J=0.1;      %Inercia 
K=0.6;      %Constante de porporcionalidad 
b=0.07;     %Coeficiente de amortiguamiento 
Ra=0.001;   %Resistenca de la armadura 
La=1.2;     %Inductancia de la armadura 
RT=R+Ra;    %Resistencia total campo-armadura(Serie)
LT=L+La;    %indictancia total campo-armadura(Serie)
% Conversiones de velocidad y voltaje  
Rpm=((1)/(2*pi))*(60);
Volt=0.017472;
%% FUNCIÓN DE TRNASFERENCIA USANDO EL METODO DE SMITH PARA EL SISTEMA SUBAMORTIGUADO
in=5; %%valor maximo de la entrada en voltios 
Ctp=118;
Cinf=80;
Mp=(Ctp-Cinf)/(Cinf);%%max sobreoscilación
tp=3.560; %%Tiempo pico
zita=0.229;%% Fctor de amortguamiento (Tabla)
wd=pi/tp;
wn=(wd)/(sqrt(1-zita^2)); %%0.9064; %% Frecuencia natural 
K1=Cinf/in; %%Ganancia del sistema 
theta=0.3;%%Tiempo muerto
num=[K1*wn^2]; 
den=[1 2*zita*wn wn^2];
Gs=tf(num,den);
Gs.inputdelay=theta;    
step(Gs,50)
hold on
%% DISCRETIZACIÓN DE LA PLANTA 
Tm= 0.356;%tp/10;
Gz=c2d(Gs,Tm,'tustin');
step(Gz,50)




