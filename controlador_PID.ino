#include <arduino-timer.h> //Libreria timer para el muestreo 
//Declaración de variables usadas en el controlado PID discreto
int i=0;
float Feedback= Serial.read(); // Lectura del valor en RPM de la salida de la planta 
float error[3];// Función que guarda los valores dentro de un arreglo (Vector) del error, error actual y error anterior 
float out [2];// Función que guarda los valores dentro de un arreglo (Vector) del error, error actual y error anterior 
//Parametros del controlador PID 
float Kp=2.45; //Ganancia proporcional 
float Ki=53.0; //Ganancia integral
float Kd=0.40; //Ganancia derivativa
float T=0.05; //Periodo de muestreo 
float K1,K2,K3; // Valo  res usados en la ecuación de diferencias posteriormente calculada 
typedef union{
  float number;
  uint8_t bytes[4];
} FLOATUNION_t;
FLOATUNION_t myValue;
Timer<1, micros> timer; // create a timer with 1 task and microsecond resolution

bool toggle_led(void *) {
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN)); // toggle the LED
  error[0]=Feedback;ú
  //Calculo de los parametros de el controlador PID en tiempo discreto 
  out[0]=K1*out[0]+K2*out[1]+K3*out[2]+out[1];
  //Ajuste de la señal 
  if(out[0]> 255.0){
    out[0]=255.0;
  }
   if(out[0]<0.0){
    out[0]=0;
  }
 
   myValue.number = out[0];
          Serial.write('A');
          for(i=0;i<4;i++){ 
    Serial.write(myValue.bytes[i]); 
          }
  // Print terminator
  Serial.print('\n');
  //corrimientos de los errores 
  error[2]=error[1];
  error[1]=error[0];
  out[1]=out[0];

  return true; // repeat? true
}
void setup() {
  Serial.begin(9600); //Comunicación serial arduino-simulink  a una velocidad de 9600 baudios 
  pinMode(LED_BUILTIN, OUTPUT); // set LED pin to OUTPUT
//Calculo de los coeficientes de la ecuación de diferencias 
K1=Kp+Ki*T*Kd/T;
K2=-2.0*Kd/T-Kp;
K3=Kd/T;
timer.every(1000, toggle_led);
}
void loop() {
  timer.tick(); // tick the timer
}
